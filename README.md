# acelee-alibaba-sentinel-apollo

#### 介绍
spring boot/spring cloud整合alibaba 的sentinel对接口进行限流，并对限流规则使用了Apollo进行了持久化，开箱即用！

#### 软件架构

spring boot 2.x 

spring cloud alibaba sentinel 0.9.0.RELEASE

apollo 1.6.0

#### 使用说明

1. 启动AceleeAlibabaSentinelApolloApplication.java类的main方法即可

#### 参与贡献

   欢迎关注star，谢谢！
   
   **使用参考**作者博文 [Spring Cloud Alibaba（5）Sentinel使用Apollo对存储规则持久化 带源码](https://blog.csdn.net/las723/article/details/90609699)