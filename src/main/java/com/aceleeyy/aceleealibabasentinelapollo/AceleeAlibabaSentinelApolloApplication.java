package com.aceleeyy.aceleealibabasentinelapollo;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableApolloConfig
@SpringBootApplication
public class AceleeAlibabaSentinelApolloApplication {

    public static void main(String[] args) {
        SpringApplication.run(AceleeAlibabaSentinelApolloApplication.class, args);
    }

}
